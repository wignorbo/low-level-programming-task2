ifeq ($(DEBUG), 1)
	FLEX_OPTS = -d
else
	FLEX_OPTS =
endif

all: compile

parser.tab.c parser.tab.h: parser.y
	bison -t -v -d parser.y -Wcounterexamples

lex.yy.c: lexer.l parser.tab.h
	flex $(FLEX_OPTS) lexer.l

compile: db_model.c printer.c query.c lex.yy.c parser.tab.c parser.tab.h
	gcc -g -I. -o parser db_model.c printer.c query.c parser.tab.c lex.yy.c -lfl

clean:
	rm parser parser.tab.c lex.yy.c parser.tab.h parser.output

.PHONY: rebuild

rebuild: | clean all